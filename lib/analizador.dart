import 'posicionamiento.dart';
import 'dart:io';

class Analizador {
  final String arquetipo;
  List<Posicionamiento> _exactas = [];
  List<Posicionamiento> _noExactas = [];

  List<Posicionamiento> get coincidenciasExactas => _exactas;
  List<Posicionamiento> get coincidenciasNoExactas => _noExactas;

  void intentarCon(String palabra) {
    _exactas =
        _obtenerCoincidenciasExactas(arquetipo: arquetipo, intento: palabra);
    _noExactas =
        _obtenerCoincidenciasNoExactas(arquetipo: arquetipo, intento: palabra);
  }

  bool get coincidenciaPerfecta =>
      _exactas.length == _obtenerPosicionamientos(palabra: arquetipo).length;
  Analizador(this.arquetipo);

  List<Posicionamiento> _obtenerPosicionamientos({required String palabra}) {
    return palabra
        .split('')
        .asMap()
        .entries
        .map((e) => Posicionamiento(posicion: e.key, letra: e.value))
        .toList();
  }

  List<Posicionamiento> _obtenerCoincidenciasExactas(
      {required String arquetipo, required String intento}) {
    var posicionamientosArquetipo =
        _obtenerPosicionamientos(palabra: arquetipo);
    var posicionamientosIntento = _obtenerPosicionamientos(palabra: intento);

    return posicionamientosArquetipo
        .toSet()
        .intersection(posicionamientosIntento.toSet())
        .toList();
  }

  List<Posicionamiento> _obtenerCoincidenciasNoExactas(
      {required String arquetipo, required String intento}) {
    var posicionamientosArquetipo =
        _obtenerPosicionamientos(palabra: arquetipo);
    var posicionamientosIntento = _obtenerPosicionamientos(palabra: intento);
    var coincidenciasExactas =
        _obtenerCoincidenciasExactas(arquetipo: arquetipo, intento: intento);

    List<Posicionamiento> coincidenciasNoExactas = [];
    for (var elemento in posicionamientosIntento) {
      coincidenciasNoExactas.addAll(posicionamientosArquetipo.where((e) {
        return e.letra == elemento.letra && !coincidenciasExactas.contains(e);
      }));
    }
    return coincidenciasNoExactas;
  }
}
