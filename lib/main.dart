import 'dart:math';

import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:widget_texto_cambiante/TODOdiplay/worlde_screen.dart';
import 'package:widget_texto_cambiante/analizador.dart';
import 'package:widget_texto_cambiante/texto.dart';
import 'teclado2.dart';

import 'bloc.dart';

void main() {
  runApp(AplicacionBloc());
}

class AplicacionBloc extends StatefulWidget {
  AplicacionBloc({Key? key}) : super(key: key);

  @override
  State<AplicacionBloc> createState() => _AplicacionBlocState();
  var analizador = Analizador("jugar");
}

class _AplicacionBlocState extends State<AplicacionBloc> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MiBloc(),
      child: MaterialApp(
        home: const MyApp(),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var analizador = Analizador("jugar");
    analizador.intentarCon("juego");
    print(analizador.coincidenciasExactas);
    print(analizador.coincidenciasNoExactas);
    int x = 0;
    return MaterialApp(
      title: 'Worlde',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(),
      home: WorldeScreen(),
    );
  }
}
