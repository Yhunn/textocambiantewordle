import 'dart:math';

import 'package:flutter/material.dart';
import 'package:widget_texto_cambiante/analizador.dart';
import 'package:widget_texto_cambiante/posicionamiento.dart';

class WidgetTexto extends StatefulWidget {
  List<Posicionamiento> exactas;
  WidgetTexto(this.exactas, {Key? key}) : super(key: key);

  @override
  State<WidgetTexto> createState() => _WidgetTextoState();
}

class _WidgetTextoState extends State<WidgetTexto> {
  @override
  Widget build(BuildContext context) {
    if (widget.exactas.isEmpty) {
      List<String> mensajesNegativos = [
        'Lo haces super mal colega',
        'Vuelve a primaria compañero',
        'Casi... pero no...',
        'chale que pena, más la que me das',
      ];
      final _random = new Random();
      return cuerpo(
          mensajesNegativos[_random.nextInt(mensajesNegativos.length)]);
    }
    if (widget.exactas!=[]) {
      List<String> mensajesPositivos = [
        'Buen trabajo, ahora no la arruines',
        'Hasta parece que estudiaste ehh',
        'Buena! ahora otra que no sea suerte!',
        'Sigue así!!!',
      ];
      final _random = new Random();
      return cuerpo(
          mensajesPositivos[_random.nextInt(mensajesPositivos.length)]);
    }
    return Center();
  }
}

class cuerpo extends StatelessWidget {
  final String mensaje;
  const cuerpo(
    this.mensaje,{
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(mensaje),
        ],
      ),
    );
  }
}