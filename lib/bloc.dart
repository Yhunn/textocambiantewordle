import 'package:bloc/bloc.dart';
import 'dart:math';

class MiBloc extends Bloc<Evento, Estado> {
  MiBloc() : super(Estado()) {
    on<EventoEmpeorar>((EventoEmpeorar event, emit) {
      List<String> mensajesNegativos = [
        'Lo haces super mal colega',
        'Vuelve a primaria compañero',
        'Casi... pero no...',
        'chale que pena, más la que me das',
      ];
      final _random = new Random();
      emit(EstadoEmpeorando(
          mensajesNegativos[_random.nextInt(mensajesNegativos.length)]));
    });
    on<EventoMejorar>((EventoMejorar event, emit) {
      List<String> mensajesPositivos = [
        'Buen trabajo, ahora no la arruines',
        'Hasta parece que estudiaste ehh',
        'Buena! ahora otra que no sea suerte!',
        'Sigue así!!!',
      ];
      final _random = new Random();
      emit(EstadoMejorando(
          mensajesPositivos[_random.nextInt(mensajesPositivos.length)]));
    });
  }
}

class Evento {}

class EventoEmpeorar extends Evento {}

class EventoMejorar extends Evento {}

class Estado {}

class EstadoEmpeorando extends Estado {
  final String mensaje;
  EstadoEmpeorando(this.mensaje);
}

class EstadoMejorando extends Estado {
  final String mensaje;
  EstadoMejorando(this.mensaje);
}
