import 'package:flutter/material.dart';

const Color correctColor = Color.fromARGB(190, 138, 224, 143);
const Color InWordColor = Color.fromARGB(230, 206, 218, 105);
const Color notInWordColor = Colors.grey;
