import 'dart:math';

import 'package:flutter/material.dart';
import 'package:widget_texto_cambiante/TODOdiplay/boards/board.dart';
import 'package:widget_texto_cambiante/TODOdiplay/colors/app_colors.dart';
import 'package:widget_texto_cambiante/teclado2.dart';
import 'package:widget_texto_cambiante/textomensajes.dart';

import 'data/word_list.dart';
import 'models/letter_model.dart';
import 'models/word_model.dart';

enum GameStatus { playing, submitting, lost, won }

List<String> mensajesNegativos = [
  'Lo haces super mal colega',
  'Vuelve a primaria compañero',
  'Casi... pero no...',
  'chale que pena, más la que me das',
];
List<String> mensajesPositivos = [
  'Buen trabajo, ahora no la arruines',
  'Hasta parece que estudiaste ehh',
  'Buena! ahora otra que no sea suerte!',
  'Sigue así!!!',
];

class WorldeScreen extends StatefulWidget {
  const WorldeScreen({Key? key}) : super(key: key);

  @override
  State<WorldeScreen> createState() => _WorldeScreenState();
}

class _WorldeScreenState extends State<WorldeScreen> {
  int _cantidadDeAciertos = 0;
  String mensajePartida = "";
  GameStatus _gameStatus = GameStatus.playing;

  final List<Word> _board = List.generate(
    6,
    (_) => Word(letters: List.generate(5, (_) => Letter.empty())),
  );

  int _currentWordIndex = 0;

  Word? get _currentWord =>
      _currentWordIndex < _board.length ? _board[_currentWordIndex] : null;

  Word _solution = Word.fromString(
    fiveLetterWords[Random().nextInt(fiveLetterWords.length)].toUpperCase(),
  );

  final Set<Letter> _letrasDelTeclado = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text(
          'Wordle',
          style: TextStyle(
            fontSize: 36,
            color: Colors.black,
            fontWeight: FontWeight.bold,
            letterSpacing: 4,
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Board(board: _board),
          const SizedBox(
            height: 80,
          ),
          panelTeclado(
            alPresionarLetra: _enTeclaPresionada,
            alPresionarEnter: _enPresionaEnter,
            alPresionarDelete: _enPresionaBorrar,
            letters: _letrasDelTeclado,
          )
        ],
      ),
    );
  }

  void _enTeclaPresionada(String valor) {
    if (_gameStatus == GameStatus.playing) {
      setState(() => _currentWord?.addLetter(valor));
    }
  }

  void _enPresionaBorrar() {
    if (_gameStatus == GameStatus.playing) {
      setState(() => _currentWord?.removeLetter());
    }
  }

  void _enPresionaEnter() {
    if (_gameStatus == GameStatus.playing &&
        !_currentWord!.letters.contains(Letter.empty())) {
      _gameStatus = GameStatus.submitting;
      int aciertosActuales = _cantidadDeAciertos;
      for (var i = 0; i < _currentWord!.letters.length; i++) {
        final currentWordLetter = _currentWord!.letters[i];
        final currentSolutionLetter = _solution!.letters[i];

        setState(() {
          if (currentWordLetter == currentSolutionLetter) {
            _currentWord!.letters[i] =
                currentWordLetter.copyWith(status: LetterStatus.correct);
            _cantidadDeAciertos++;
          } else if (_solution.letters.contains(currentWordLetter)) {
            _currentWord!.letters[i] =
                currentWordLetter.copyWith(status: LetterStatus.inWord);
            _cantidadDeAciertos++;
          } else {
            _currentWord!.letters[i] =
                currentWordLetter.copyWith(status: LetterStatus.notInWord);
          }
        });

        final letra = _letrasDelTeclado.firstWhere(
            (e) => e.val == currentWordLetter.val,
            orElse: () => Letter.empty());
        if (letra.status != LetterStatus.correct) {
          _letrasDelTeclado.removeWhere((e) => e.val == currentWordLetter.val);
          _letrasDelTeclado.add(_currentWord!.letters[i]);
        }
      }

      _generaMensaje(aciertosActuales, _cantidadDeAciertos);
      _GanaroPerder();
    }
  }

  void _generaMensaje(int anteriores, int ACPosteriores) {
    String mensajeDeSalida = "";
    final _random = new Random();
    Color colorDeSalida;
    if (anteriores >= ACPosteriores) {
      mensajeDeSalida =
          mensajesNegativos[_random.nextInt(mensajesNegativos.length)];
      colorDeSalida = Colors.red;
    } else {
      mensajeDeSalida =
          mensajesPositivos[_random.nextInt(mensajesNegativos.length)];
      colorDeSalida = correctColor;
    }
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        dismissDirection: DismissDirection.down,
        duration: const Duration(seconds: 5),
        backgroundColor: colorDeSalida,
        content: Text(mensajeDeSalida, style: TextStyle(color: Colors.white)),
        action: SnackBarAction(
          onPressed: () {},
          label: "OK",
          textColor: Colors.white,
        ),
      ),
    );
  }

  void _GanaroPerder() {
    if (_currentWord!.wordString == _solution.wordString) {
      _gameStatus = GameStatus.won;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          dismissDirection: DismissDirection.none,
          duration: const Duration(days: 1),
          backgroundColor: correctColor,
          content: const Text(
            'Ganaste!!',
            style: TextStyle(color: Colors.white),
          ),
          action: SnackBarAction(
            onPressed: _reinciar,
            textColor: Colors.white,
            label: 'Nuevo Juego',
          ),
        ),
      );
    } else if (_currentWordIndex + 1 >= _board.length) {
      _gameStatus = GameStatus.lost;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          dismissDirection: DismissDirection.none,
          duration: const Duration(days: 1),
          backgroundColor: Colors.red,
          content: Text(
            'Perdiste!!, La Palabra Correcta era: ${_solution.wordString}',
            style: const TextStyle(color: Colors.white),
          ),
          action: SnackBarAction(
            onPressed: _reinciar,
            textColor: Colors.white,
            label: 'Quieres Intentarlo de nuevo??',
          ),
        ),
      );
    } else {
      _gameStatus = GameStatus.playing;
    }
    _currentWordIndex += 1;
  }

  void _reinciar() {
    setState(() {
      _gameStatus = GameStatus.playing;
      _currentWordIndex = 0;
      _board
        ..clear()
        ..addAll(
          List.generate(
            6,
            (_) => Word(letters: List.generate(5, (_) => Letter.empty())),
          ),
        );
      _solution = Word.fromString(
        fiveLetterWords[Random().nextInt(fiveLetterWords.length)].toUpperCase(),
      );
      _letrasDelTeclado.clear();
    });
  }
}
