import 'package:flutter/material.dart';

import 'TODOdiplay/models/letter_model.dart';

const _teclas = [
  ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
  ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
  ['ENTER', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'DEL'],
];

class panelTeclado extends StatelessWidget {
  const panelTeclado({
    Key? key,
    required this.alPresionarLetra,
    required this.alPresionarEnter,
    required this.alPresionarDelete,
    required this.letters,
  }) : super(key: key);

  final void Function(String) alPresionarLetra;
  final VoidCallback alPresionarEnter;
  final VoidCallback alPresionarDelete;
  final Set<Letter> letters;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: _teclas
          .map((filaTeclas) => Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: filaTeclas.map((letra) {
                  if (letra == "DEL") {
                    return Tecla.delete(
                      accion: alPresionarDelete,
                    );
                  } else if (letra == "ENTER") {
                    return Tecla.enter(
                      accion: alPresionarEnter,
                    );
                  }

                  final letraTeclado = letters.firstWhere(
                    (e) => e.val == letra,
                    orElse: () => Letter.empty(),
                  );

                  return Tecla(
                      accion: () => alPresionarLetra(letra),
                      letra: letra,
                      colorDeFondo: letraTeclado != Letter.empty()
                          ? letraTeclado.backgroundColor
                          : Colors.grey);
                }).toList(),
              ))
          .toList(),
    );
  }
}

class Tecla extends StatelessWidget {
  const Tecla({
    Key? key,
    this.alto = 48,
    this.largo = 30,
    required this.accion,
    required this.colorDeFondo,
    required this.letra,
  }) : super(key: key);

  factory Tecla.delete({required VoidCallback accion}) => Tecla(
        accion: accion,
        colorDeFondo: Colors.grey,
        letra: "DEL",
        largo: 56,
      );
  factory Tecla.enter({required VoidCallback accion}) => Tecla(
        accion: accion,
        colorDeFondo: Colors.grey,
        letra: "ENTER",
        largo: 56,
      );

  final double alto;
  final double largo;
  final VoidCallback accion;
  final Color colorDeFondo;
  final String letra;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 2.0),
      child: Material(
        color: colorDeFondo,
        borderRadius: BorderRadius.circular(4),
        child: InkWell(
          onTap: accion,
          child: Container(
              height: alto,
              width: largo,
              alignment: Alignment.center,
              child: Text(
                letra,
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
              )),
        ),
      ),
    );
  }
}
